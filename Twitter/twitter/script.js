
function TwitterController($scope, $http) {
    
    loadRecords($scope, $http);

    $scope.save = function () {

	    var Mensagem = {
	    		id: $scope.msg_id,
	            conteudo: $scope.conteudo
	        };
    	
    	if (Mensagem.conteudo != null && Mensagem.conteudo != "") {

		    $http({
		        url: 'http://localhost:8080/twitter_rest/crud.php/mensagem_add',
		        dataType: 'json',
		        method: 'POST',
		        data: Mensagem,
		        headers: {
		            "Content-Type": "application/json"
		        }
		    }).success(function(response){
		        $scope.response = response;
		        loadRecords($scope, $http);
		        $scope.conteudo = "";
		    }).error(function(error){
		        $scope.error = Alert(error);
		    });	 
	    };   
	};

	$scope.delete = function (idx) {
       $http({
	        url: 'http://localhost:8080/twitter_rest/crud.php/delete_mensagem/' + $scope.mensagens[idx].id,
	        dataType: 'json',
	        method: 'DELETE',
	        headers: {
	            "Content-Type": "application/json"
	        }
	    }).success(function(response){
	        $scope.response = response;
	        loadRecords($scope, $http);
	        $scope.conteudo = "";
	    }).error(function(error){
	        $scope.error = Alert(error);
	    });	 
    };

    function loadRecords($scope, $http) {
	    $http.get('http://localhost:8080/twitter_rest/crud.php').
	        success(function(data) {
	        	$scope.mensagens = data;
	        });
	}
}