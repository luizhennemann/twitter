<?php

require 'vendor/autoload.php';
require 'lib/mysql.php';

$app = new Slim\App();

$app->get('/', 'get_mensagem');

$app->get('/mensagem/{id}', function($request, $response, $args) {
    get_mensagem_id($args['id']);
});
$app->post('/mensagem_add', function($request, $response, $args) {
    add_mensagem($request->getParsedBody());
});
$app->delete('/delete_mensagem/{id}', function($request, $response, $args) {
    delete_mensagem_id($args['id']);
});
$app->run();

function get_mensagem() {
    $db = connect_db();
    $sql = "SELECT * FROM mensagens ORDER BY `data_msg` DESC";
    $exe = $db->query($sql);
    $data = [];
    while ($row = $exe->fetch_assoc()) {
        $data[] = $row;
    }
    $db = null;
    echo json_encode($data);
}

function get_mensagem_id($mensagem_id) {
    $db = connect_db();
    $sql = "SELECT * FROM mensagens WHERE `id` = '$mensagem_id'";
    $exe = $db->query($sql);
    $data = [];
    while ($row = $exe->fetch_assoc()) {
        $data[] = $row;
    }
    $db = null;
    echo json_encode($data);
}

function add_mensagem($data) {
    $db = connect_db();
    $sql = "insert into mensagens (conteudo)"
            . " VALUES('$data[conteudo]')";
    $exe = $db->query($sql);
    $last_id = $db->insert_id;
    $db = null;
    if (!empty($last_id))
        echo $last_id;
    else
        echo false;
}

function delete_mensagem_id($mensagem_id) {
    $db = connect_db();
    $sql = "DELETE FROM mensagens WHERE id = '$mensagem_id'";
    $exe = $db->query($sql);
    $db = null;
    if (!empty($last_id))
        echo $last_id;
    else
        echo false;
}
?>