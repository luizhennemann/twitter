<?php
require 'vendor/autoload.php';
 
$app = new Slim\App();

$app->get('/', function ($request, $response, $args) { 
	$response->write("Api mensagens Twitter");
	return $response;
});

$app->get('/mensagem', function ($request, $response, $args) {
	$response->write("Minhas mensagens!");
	return $response;
});

$app->run();
?>