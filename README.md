# README #

Aplicação desenvolvida para atender a solicitação de desafio da empresa Social Base.

# CRIAR BANCO DE DADOS #

Para rodar a aplicação, deve-se criar o banco de dados sb_twitter e importar o script 'sb_twitter.sql'. Feito isso, deve-se alterar o arquivo 'twitter_rest\lib\mysql.php' para especificar os dados de conexão da base de dados.

# EXECUTAR A APLICAÇÃO #

Para executar a aplicação, basta incluir os arquivos no servidor de preferência e alterar as URL's no arquivo 'twitter\script.js' para apontar para o servidor onde a aplicação está instalada.